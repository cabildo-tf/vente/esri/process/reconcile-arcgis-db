import os
import yaml


def load_config(path):
    if path and os.path.exists(path):
        with open(path, 'r') as f:
            return yaml.safe_load(f.read())