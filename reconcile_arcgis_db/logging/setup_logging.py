import logging.config
import os
import pathlib

import yaml
from logging.handlers import RotatingFileHandler as RotatingFileBaseHandler


class SetupLogging:
    def __init__(self, *args, **kwargs):
        self.default_config = kwargs.pop("logging_path", None)
        if not self.default_config:
            root_path = os.path.dirname(__file__)
            self.default_config = os.path.join(root_path, "logging.yml")

    def setup_logging(self, default_level=logging.INFO, env_key='LOG_CFG'):
        path = os.getenv(env_key, self.default_config)
        if os.path.exists(path):
            with open(path, 'r') as f:
                config = yaml.safe_load(f.read())
                logging.config.dictConfig(config)
        else:
            logging.info('Error in loading logging configuration. Using default configs')
            logging.basicConfig(level=default_level)


class RotatingFileHandler(RotatingFileBaseHandler):
    def __init__(self, filename, mode='a', maxBytes=0, backupCount=0, encoding=None, delay=False):
        pathlib.Path(os.path.dirname(filename)).mkdir(parents=True, exist_ok=True)
        RotatingFileBaseHandler.__init__(self, filename, mode, maxBytes, backupCount, encoding, delay)
