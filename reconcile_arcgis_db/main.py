import argparse
import os
import logging
import arcpy
import yaml
from prometheus_client import Gauge, CollectorRegistry, push_to_gateway
from reconcile_arcgis_db.logging.setup_logging import SetupLogging
from reconcile_arcgis_db.prometheus import basic_auth_handler
from reconcile_arcgis_db.utils import load_config
from datetime import datetime


registry = CollectorRegistry()
STAGE_TIME = Gauge('arcgis_reconcile_db_seconds', 'Time spent in reconcile arcgis database', registry=registry,
                   labelnames=['stage'])
STAGE_TIME_RECONCILE = STAGE_TIME.labels(stage='reconcile')
STAGE_TIME_RECONCILIATE = STAGE_TIME.labels(stage='reconciliate')
STAGE_TIME_COMPRESS = STAGE_TIME.labels(stage='compress')
STAGE_TIME_REBUILD = STAGE_TIME.labels(stage='rebuild')

DATETIME_EXECUTION = Gauge('arcgis_reconcile_db_executed_date_seconds', 'Last time job reconcile successfully finished',
                           registry=registry)


def main():
    args = get_arguments()
    SetupLogging(logging_path=args.logging_conf_path).setup_logging()
    try:
        connection_db = make_process_to_get_connection(args.db_config_path, args.sde_config_path, args.delete_config_db)
        if exists_users_connected(connection_db):
            disconect_users(connection_db)
        config_reconcile = get_config_yaml(args.parameters_conf_path) if args.parameters_conf_path else {}
        reconcile(connection_db, config_reconcile)
    except Exception as err:
        logging.error(err)
    finally:
        if args.pushgateway_config_path:
            pushgateway_config = get_config_yaml(args.pushgateway_config_path)
            DATETIME_EXECUTION.set_to_current_time()
            send_metrics(**pushgateway_config)
            logging.info('Execution finished')


def get_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--db-config", dest="db_config_path")
    parser.add_argument("-s", "--sde-config", dest="sde_config_path")
    parser.add_argument("-r", "--delete-config-db", type=bool, default=True,
                        help="Delete DB config file", dest="delete_config_db")
    parser.add_argument("-l", "--logging", dest="logging_conf_path", default=None)
    parser.add_argument("-c", "--config", dest="parameters_conf_path", default=None)
    parser.add_argument("-p", "--pushgateway-config", dest="pushgateway_config_path", default=None)
    return parser.parse_args()


def make_process_to_get_connection(db_config_path, sde_config_path, delete_config_db):
    if db_config_path and os.path.isfile(db_config_path):
        db_config = load_config(db_config_path)
        if delete_config_db and os.path.exists(db_config_path):
            os.remove(db_config_path)
        connection_db = db_connection(db_config)
    elif sde_config_path:
        connection_db = sde_config_path
    else:
        raise Exception("No file connection_db")
    return connection_db


def db_connection(config):
    logging.info("Obtaining db connection config")
    if all(k in config for k in ('out_folder_path', 'out_name')):
        sde_path = config['out_folder_path'] + "/" + config['out_name']
        if os.path.exists(sde_path):
            os.remove(sde_path)
        arcpy.CreateDatabaseConnection_management(**config)
        return config['out_folder_path'] + "/" + config['out_name']
    else:
        raise Exception("Config file not exists or it is wrong")


def exists_users_connected(connection_db):
    try:
        users_list = arcpy.ListUsers(connection_db)
        logging.info(f"Total users connected: {len(users_list)}")
        return len(users_list)
    except Exception as err:
        logging.error(err)
        raise Exception("Error checking users connected")


def disconect_users(config):
    try:
        arcpy.DisconnectUser(config, "ALL")
    except Exception as err:
        logging.debug(err)
        raise Exception("Error disconnecting users connected")


def get_config_yaml(parameters_conf_path):
    if parameters_conf_path and os.path.exists(parameters_conf_path):
        with open(parameters_conf_path, 'r') as f:
            return yaml.safe_load(f.read())
    else:
        logging.info('Using default configs')


@STAGE_TIME_RECONCILE.time()
def reconcile(connection_db, config_reconcile_yaml):
    try:
        block_new_db_connections(connection_db, False)
        versions = get_reconciliate_versions(connection_db)
        if len(versions):
            reconciliate_versions(connection_db=connection_db, versions=versions, **config_reconcile_yaml)
            compress_database(connection_db)
            rebuild_indexes_and_update_statistics(connection_db, "SYSTEM")
    except Exception as err:
        logging.debug(err)
        raise Exception("Error in reconcile process")
    finally:
        block_new_db_connections(connection_db, True)


def block_new_db_connections(connection_db, block):
    try:
        arcpy.AcceptConnections(connection_db, block)
        if block is True:
            logging.info("New connections accepted")
        elif block is False:
            logging.info("New connections blocked")
    except Exception as err:
        logging.error(err)
        if block is True:
            raise Exception("Error enabling new db connections")
        elif block is False:
            raise Exception("Error blocking new db connections")


def get_reconciliate_versions(connection_db):
    try:
        logging.info("Compiling the list of versions to reconciliate")
        ver_list = arcpy.da.ListVersions(connection_db)
        versions = [ver.name for ver in ver_list if ver.parentVersionName == 'sde.DEFAULT']
        logging.info(f"Total of reconciliate versions: {len(versions)}")
        return versions
    except Exception as err:
        logging.error(err)
        raise Exception("Error getting reconciliate versions")


@STAGE_TIME_RECONCILIATE.time()
def reconciliate_versions(connection_db, versions, *args, **kwargs):
    reconcile_mode = kwargs.pop("reconcile_mode", "ALL_VERSIONS")
    target_version = kwargs.pop("target_version", "sde.DEFAULT")
    acquire_locks = kwargs.pop("acquire_locks", "NO_LOCK_ACQUIRED")
    abort_if_conflicts = kwargs.pop("abort_if_conflicts", "NO_ABORT")
    conflict_definition = kwargs.pop("conflict_definition", "BY_OBJECT")
    conflict_resolution = kwargs.pop("conflict_resolution", "FAVOR_EDIT_VERSION")
    with_post = kwargs.pop("with_post", "POST")
    with_delete = kwargs.pop("with_delete", "KEEP_VERSION")
    output_log = kwargs.pop("output_log", generate_file_name())
    try:
        logging.info("Starting to reconciliate the listed versions")
        arcpy.ReconcileVersions_management(connection_db, reconcile_mode, target_version, versions, acquire_locks,
                                           abort_if_conflicts, conflict_definition, conflict_resolution, with_post,
                                           with_delete, output_log)
        logging.info("Reconciliating versions process finished")
    except Exception as err:
        logging.error(err)
        raise Exception("Error reconciliating versions")


def generate_file_name(preffix="./logs/", suffix="reconcile.log"):
    now = datetime.now().strftime("%Y-%m-%d_%H_%M")
    file_name = f"{preffix}{now}-{suffix}"
    return file_name


@STAGE_TIME_COMPRESS.time()
def compress_database(config):
    try:
        arcpy.Compress_management(config)
        logging.info("Compressing databse process finished")
    except Exception as err:
        logging.error(err)
        raise Exception("Error compressing database")


@STAGE_TIME_REBUILD.time()
def rebuild_indexes_and_update_statistics(config, system='SYSTEM'):
    try:
        arcpy.RebuildIndexes_management(config, system)
        arcpy.AnalyzeDatasets_management(config, system)
        logging.info("Rebuild indexes and update statistics process finished")
    except Exception as err:
        logging.error(err)
        raise Exception("Error rebuilding indexes and updating statistics")


def send_metrics(*args, **kwargs):
    def my_auth_handler(url, method, timeout, headers, data):
        username = kwargs.pop("username", None)
        password = kwargs.pop("password", None)
        return basic_auth_handler(url, method, timeout, headers, data, username, password)

    job = kwargs.pop("job", "reconcile-arcgis-db")
    gateway = kwargs.pop("gateway", "localhost:9000")
    try:
        logging.info('Starting process of pushing metrics')
        push_to_gateway(registry=registry, handler=my_auth_handler, gateway=gateway, job=job)
    except Exception as err:
        logging.error(err)


if __name__ == '__main__':
    main()
