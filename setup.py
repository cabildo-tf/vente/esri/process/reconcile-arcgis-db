import setuptools

setuptools.setup(
    name="reconcile_arcgis_db",
    version="0.0.4",
    author="David Pulido",
    author_email="dpulmac@gesplan.es",
    description="Conciliate and publish versions",
    packages=setuptools.find_packages(),
    include_package_data=True,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    install_requires=[
          'pyyaml'
      ],
    entry_points={
        'console_scripts': ['reconcile-arcgis-db=reconcile_arcgis_db.main:main'],
    }
)
