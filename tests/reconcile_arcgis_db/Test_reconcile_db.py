import argparse
import logging
import os
import sys
import tempfile
from collections import namedtuple
from unittest import TestCase
from unittest.mock import call
from mock import patch, MagicMock
from nose.tools import eq_, ok_
from datetime import datetime

from reconcile_arcgis_db.logging.setup_logging import SetupLogging


if 'arcpy' not in sys.modules.keys():
    arcpy_module = MagicMock()
    sys.modules["arcpy"] = arcpy_module

from reconcile_arcgis_db.main import exists_users_connected, get_reconciliate_versions, block_new_db_connections,\
    reconciliate_versions, reconcile, db_connection, get_arguments, compress_database, \
    rebuild_indexes_and_update_statistics, make_process_to_get_connection, disconect_users


class TestArcpyWrapper(TestCase):
    @patch('arcpy.CreateDatabaseConnection_management')
    @patch('reconcile_arcgis_db.main.db_connection', MagicMock(return_value='out_folder_path/name'))
    def test_return_ConfigFile_When_Function_Db_ConnectionIsCalled(self, mock_arcpy):
        conf = {'out_folder_path': 'out_folder_path', 'out_name': 'name'
                }
        path = db_connection(conf)
        eq_(path, 'out_folder_path/name')

    @patch('arcpy.CreateDatabaseConnection_management')
    @patch('reconcile_arcgis_db.main.db_connection', MagicMock(return_value='./tests/resources/prueba.sde'))
    def test_return_ConfigFile_And_Delete_SDEFile_When_Function_Db_ConnectionIsCalledAndUrlSdeExists(self, mock_arcpy):
        conf = {'out_folder_path': './tests/resources', 'out_name': 'prueba.sde'
                }
        path_sde = f"{conf['out_folder_path']}/{conf['out_name']}"
        if os.path.exists(conf['out_folder_path']):
            if not os.path.exists(path_sde):
                open(path_sde, 'w').close()
            path = db_connection(conf)
        eq_(path, f"{conf['out_folder_path']}/{conf['out_name']}")
        ok_(not os.path.exists(f"{conf['out_folder_path']}/{conf['out_name']}"))

    @patch('arcpy.CreateDatabaseConnection_management')
    @patch('reconcile_arcgis_db.main.db_connection', MagicMock(return_value='./tests/resources/prueba.sde'))
    def test_return_ConfigFile_When_Function_Db_ConnectionIsCalledAndSdeisDeleted(self, mock_arcpy):
        conf = {'out_folder_path': './tests/resources', 'out_name': 'prueba.sde'
                }
        path = db_connection(conf)
        eq_(path, f"{conf['out_folder_path']}/{conf['out_name']}")
        ok_(not os.path.exists(f"{conf['out_folder_path']}/{conf['out_name']}"))

    @patch('arcpy.ListUsers', MagicMock(return_value=['user1', 'user2']))
    def test_returnNumberOfUsers_When_ThereAreUsersConnected(self):
        users = exists_users_connected('conf')
        eq_(users, 2)

    @patch('arcpy.ListUsers', MagicMock(return_value=[]))
    def test_returnZero_When_ThereAreNotUsersConnected(self):
        users = exists_users_connected('conf')
        eq_(users, 0)


    @patch('arcpy.ListUsers', MagicMock(side_effect=Exception()))
    def test_returnException_When_FunctionExistsUsersRaisesAnException(self):
        self.assertRaises(Exception, exists_users_connected, 'conf')

    @patch('arcpy.da.ListVersions')
    def test_returnStringOfVersions_When_ThereAreVersionsToReconciliate(self, mock_arcpy):
        ver1 = {'name': 'Version1', 'parentVersionName': 'sde.DEFAULT'}
        version = namedtuple("ObjectName", ver1.keys())(*ver1.values())
        mock_arcpy.return_value = [version]
        versions_to_reconciliate = get_reconciliate_versions('conf')
        eq_(versions_to_reconciliate, ['Version1'])

    @patch('arcpy.da.ListVersions', MagicMock(return_value=""))
    def test_returnEmptyStringOfVersions_When_ThereAreNotVersionsToReconciliate(self):
        versions = get_reconciliate_versions('conf')
        eq_(versions, [])

    @patch('arcpy.da.ListVersions', MagicMock(side_effect=Exception()))
    def test_returnException_When_FunctionGetReconciliateVersionsRaisesAnException(self):
        self.assertRaises(Exception, get_reconciliate_versions, 'conf')

    @patch('arcpy.AcceptConnections')
    def test_CalledOnceWithFalse_When_BlockDbConnections(self, mock_arcpy):
        block_new_db_connections('conf', False)
        mock_arcpy.assert_called_once_with('conf', False)

    @patch('arcpy.AcceptConnections')
    def test_CalledOnceWithTrue_When_NotBlockDbConnections(self, mock_arcpy):
        block_new_db_connections('conf', True)
        mock_arcpy.assert_called_once_with('conf', True)

    @patch('arcpy.AcceptConnections', MagicMock(side_effect=Exception()))
    def test_returnException_When_FunctionBlockNewDbConnectionsRaisesAnException(self):
        with self.assertRaises(Exception):
            block_new_db_connections('conf', False)

    @patch('arcpy.ReconcileVersions_management', MagicMock(side_effect=Exception()))
    def test_returnException_When_FunctionReconciliateVersionsRaisesAnException(self):
        versions = ['Version1', 'Version2']
        with self.assertRaises(Exception):
            reconciliate_versions('conf', versions, "output_log_path")

    @patch('arcpy.ReconcileVersions_management')
    def test_CalledOnceReconcileVersions_When_FunctionReconciliateVersionsIsCalledWithVersionsAndOutput(self, mock_arcpy):
        versions = ['Version1', 'Version2']
        reconcile_mode = "ALL_VERSIONS"
        target_version = "sde.DEFAULT"
        acquire_locks = "NO_LOCK_ACQUIRED"
        abort_if_conflicts = "NO_ABORT"
        conflict_definition = "BY_OBJECT"
        conflict_resolution = "FAVOR_EDIT_VERSION"
        with_post = "POST"
        with_delete = "KEEP_VERSION"
        reconciliate_versions('conf', versions, 'output_log_path')
        preffix = "./logs/"
        suffix="reconcile.log"
        now = datetime.now().strftime("%Y-%m-%d_%H_%M")
        output_log = f"{preffix}{now}-{suffix}"
        mock_arcpy.assert_called_once_with('conf', reconcile_mode, target_version, versions, acquire_locks,
                                           abort_if_conflicts, conflict_definition, conflict_resolution, with_post,
                                           with_delete, output_log)

    @patch('reconcile_arcgis_db.main.block_new_db_connections', MagicMock(side_effect=Exception()))
    def test_returnException_When_FunctionReconcileRaisesAnException(self):
        self.assertRaises(Exception, reconcile, 'conf')

    @patch('arcpy.Compress_management')
    def test_functionCompressManagementCalledOnce_When_FunctionCompressDbIsCalledWithConf(self, mock_arcpy):
        compress_database('conf')
        mock_arcpy.assert_called_once_with('conf')

    @patch('arcpy.Compress_management', MagicMock(side_effect=Exception()))
    def test_functionCompressManagementReturnException_When_FunctionCompressDbRaisesAnException(self):
        self.assertRaises(Exception, compress_database, 'conf')

    @patch('arcpy.DisconnectUser')
    def test_functionDisconnectUsersCalledOnce_When_FunctionDisconnectUsersIsCalledWithConf(self, mock_arcpy):
        disconect_users('conf')
        mock_arcpy.assert_called_once_with('conf', "ALL")

    @patch('arcpy.DisconnectUser', MagicMock(side_effect=Exception()))
    def test_functionDisconnectUsersReturnException_When_FunctionArcpyDisconnectUserRaisesAnException(self):
        self.assertRaises(Exception, disconect_users, 'conf')

    @patch('arcpy.RebuildIndexes_management')
    @patch('arcpy.AnalyzeDatasets_management')
    def test_CalledOnceRebuildIndexesAndUpdateStatistics_When_FunctionRebuildAndUpdateIsCalled(self, mock_arcpy_rebuild,
                                                                                               mock_arcpy_analyze):
        rebuild_indexes_and_update_statistics('conf', system='SYSTEM')
        system = "SYSTEM"
        mock_arcpy_rebuild.assert_called_once_with('conf', system)
        mock_arcpy_analyze.assert_called_once_with('conf', system)

    @patch('arcpy.RebuildIndexes_management', MagicMock(side_effect=Exception()))
    def test_returnException_When_FunctionRebuildAndIndexRaisesAnException(self):
        system = "SYSTEM"
        with self.assertRaises(Exception):
            rebuild_indexes_and_update_statistics('conf', system)

    @patch('reconcile_arcgis_db.main.block_new_db_connections')
    @patch('reconcile_arcgis_db.main.get_reconciliate_versions')
    @patch('reconcile_arcgis_db.main.reconciliate_versions')
    @patch('reconcile_arcgis_db.main.compress_database')
    @patch('reconcile_arcgis_db.main.rebuild_indexes_and_update_statistics')
    def test_CalledOnceAllFunctionsInReconcile_When_ThereAreVersionsToReconcile(self, mock_rebuild, mock_compress, mock_reconciliate_versions,
                                                                     mock_get_reconciliate_versions, mock_new_db_connections):
        versions = ['Version1', 'Version2']
        mock_get_reconciliate_versions.return_value = versions
        config_reconcile_yaml = {}
        reconcile('conf', config_reconcile_yaml)
        mock_new_db_connections.assert_has_calls([call('conf', False), call('conf', True)])
        mock_get_reconciliate_versions.assert_called_once_with('conf')
        mock_reconciliate_versions.assert_called_once_with(connection_db='conf', versions=versions)
        mock_compress.assert_called_once_with('conf')
        system = "SYSTEM"
        mock_rebuild.assert_called_once_with('conf', system)

    @patch('reconcile_arcgis_db.main.block_new_db_connections')
    @patch('reconcile_arcgis_db.main.get_reconciliate_versions')
    def test_CalledOnceAllFunctionsInReconcile_When_ThereAreNotVersionsToReconcile(self, mock_get_reconciliate_versions,
                                                                                mock_new_db_connections):
        versions = []
        mock_get_reconciliate_versions.return_value = versions
        config_reconcile_yaml = {}
        reconcile('conf', config_reconcile_yaml)
        mock_new_db_connections.assert_has_calls([call('conf', False), call('conf', True)])
        mock_get_reconciliate_versions.assert_called_once_with('conf')

    @patch('reconcile_arcgis_db.main.make_process_to_get_connection', MagicMock(return_value='conf'))
    @patch('argparse.ArgumentParser.parse_args', MagicMock(
        return_value=argparse.Namespace(db_config_path=tempfile.NamedTemporaryFile(delete=False).name,
                                        delete_config_db=False, sde_config_path="conf",
                                        logging_conf_path='logging.yml')))
    @patch('reconcile_arcgis_db.main.db_connection', MagicMock(return_value='conf'))
    def test_CalledOnceAllFunctionsInConfigDbProcess_When_MakeDbProcessIscalledWithSdeConfig(self):
        args = argparse.ArgumentParser().parse_args()
        connection_db = make_process_to_get_connection(args.db_config_path, args.sde_config_path,
                                                           args.delete_config_db)
        eq_(connection_db, "conf")
        ok_(os.path.exists(args.db_config_path))

    @patch('reconcile_arcgis_db.main.make_process_to_get_connection', MagicMock(return_value='conf'))
    @patch('argparse.ArgumentParser.parse_args', MagicMock(
        return_value=argparse.Namespace(db_config_path=tempfile.NamedTemporaryFile(delete=False).name,
                                        delete_config_db=False, sde_config_path="conf",
                                        logging_conf_path='logging.yml')))
    @patch('reconcile_arcgis_db.main.db_connection', MagicMock(return_value='conf'))
    def test_CalledOnceAllFunctionsInConfigDbProcess_When_MakeDbProcessIscalledWithDbConfigButNotDelete(self):
        args = argparse.ArgumentParser().parse_args()
        connection_db = make_process_to_get_connection(args.db_config_path, args.sde_config_path, args.delete_config_db)
        eq_(connection_db, "conf")
        ok_(os.path.exists(args.db_config_path))

    @patch('reconcile_arcgis_db.main.make_process_to_get_connection', MagicMock(return_value='conf'))
    @patch('argparse.ArgumentParser.parse_args', MagicMock(
        return_value=argparse.Namespace(db_config_path=tempfile.NamedTemporaryFile(delete=False).name,
                                        delete_config_db=True, sde_config_path="", logging_conf_path='logging.yml')))
    @patch('reconcile_arcgis_db.main.db_connection', MagicMock(return_value='conf'))
    def test_CalledOnceAllFunctionsInConfigDbProcess_When_MakeDbProcessIscalledWithDbConfigAndDelete(self):
        args = argparse.ArgumentParser().parse_args()
        connection_db = make_process_to_get_connection(args.db_config_path, args.sde_config_path, args.delete_config_db)
        eq_(connection_db, "conf")
        ok_(not os.path.exists(args.db_config_path))

    @patch('reconcile_arcgis_db.main.make_process_to_get_connection', MagicMock(side_effect=Exception()))
    def test_InConfigDbProcess_When_RaisesAnException(self):
        parser = argparse.ArgumentParser()
        parser.add_argument("-d", "--db-config", dest="db_config_path")
        parser.add_argument("-s", "--sde-config", dest="sde_config_path")
        parser.add_argument("-r", "--delete-config-db", type=bool, default=True,
                            help="Delete DB config file", dest="delete_config_db")
        parser.add_argument("-l", "--logging", dest="logging_conf_path")
        parser.add_argument("-c", "--config", dest="parameters_conf_path")
        args = parser.parse_args()
        self.assertRaises(Exception, make_process_to_get_connection, args)

    def test_Logs(self):
        SetupLogging(logging_path="./tests/resources/logging.yml").setup_logging()
        logging.info("Test log")

    @patch('reconcile_arcgis_db.main.get_arguments')
    def test_returnArguments_WHEN_functionGetArgumentsIsCalled(self, mock_get_args):
        parser = argparse.ArgumentParser()
        parser.add_argument("-d", "--db-config", dest="db_config_path")
        parser.add_argument("-s", "--sde-config", dest="sde_config_path")
        parser.add_argument("-r", "--delete-config-db", type=bool, default=True,
                            help="Delete DB config file", dest="delete_config_db")
        parser.add_argument("-l", "--logging", dest="logging_conf_path")
        parser.add_argument("-c", "--config", dest="parameters_conf_path")
        mock_get_args.return_value = parser.parse_args()
        args = get_arguments()
        eq_(args.db_config_path, None)
        eq_(args.sde_config_path, None)
        eq_(args.delete_config_db, True)
