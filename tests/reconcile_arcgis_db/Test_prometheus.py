import argparse
import os
import shutil
import sys
import time
from collections import namedtuple
from unittest import TestCase
from mock import patch


class ArcpyDaFake(object):
    @staticmethod
    def ListVersions(*args, **kwargs):
        ver1 = {'name': 'Version1', 'parentVersionName': 'sde.DEFAULT'}
        version = namedtuple("ObjectName", ver1.keys())(*ver1.values())
        return [version]


class ArcpyFake(object):
    da = ArcpyDaFake()

    @staticmethod
    def CreateDatabaseConnection_management(*args, **kwargs):
        time.sleep(1)
        return "Create Database"

    @staticmethod
    def DisconnectUser(*args, **kwargs):
        return "Users disconnected"

    @staticmethod
    def ListUsers(*args, **kwargs):
        return ['user1', 'user2']

    @staticmethod
    def AcceptConnections(*args, **kwargs):
        return "Connections blocked or accepted"

    @staticmethod
    def ReconcileVersions_management(*args, **kwargs):
        return ['Version1', 'Version2']

    @staticmethod
    def Compress_management(*args, **kwargs):
        time.sleep(0.4)
        return "Compressing db"

    @staticmethod
    def RebuildIndexes_management(*args, **kwargs):
        time.sleep(1)
        return "Rebuilding indexes"

    @staticmethod
    def AnalyzeDatasets_management(*args, **kwargs):
        return "Analyzing datasets"


sys.modules["arcpy"] = ArcpyFake

from reconcile_arcgis_db.main import main


class TestArcpyWrapper(TestCase):
    def tearDown(self) -> None:
        tmp_dir = "./tests/tmp"
        shutil.rmtree(tmp_dir)

    @patch('argparse.ArgumentParser.parse_args',
           return_value=argparse.Namespace(db_config_path='./tests/resources/db_config.yaml', delete_config_db=False,
                                           sde_config_path=None, logging_conf_path='./tests/resources/logging.yml',
                                           parameters_conf_path='./tests/resources/config.yaml',
                                           pushgateway_config_path='./tests/resources/pushgateway.yml'))
    def test_CreateDatabase(self, mock_get_arguments):
        main()
