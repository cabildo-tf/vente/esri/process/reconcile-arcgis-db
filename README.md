# Reconcile ArcGIS DB

[![pipeline status](https://gitlab.com/cabildo-tf/vente/esri/process/reconcile-arcgis-db/badges/master/pipeline.svg)](https://gitlab.com/cabildo-tf/vente/esri/process/reconcile-arcgis-db/-/commits/master)
[![coverage report](https://gitlab.com/cabildo-tf/vente/esri/process/reconcile-arcgis-db/badges/master/coverage.svg)](https://gitlab.com/cabildo-tf/vente/esri/process/reconcile-arcgis-db/-/commits/master)

Este Script realiza el proceso de conciliación y publicación de la base de datos de ArcGIS. Para ello, realiza los siguientes pasos:
1) Obtiene las rutas de todos los ficheros de configuración utilizados y genera el directorio para los logging.
2) Obtiene los datos de la conexión de base de datos que se utilizarán en las llamadas posteriores.
3) Si existen usuarios conectados a la BBDD se desconectan
4) Se inicia el proceso de reconciliación, para ello:
    - Se bloquean nuevas conexiones a la BBDD
    - Se obtiene el listado de versiones a reconciliar.
    - Si existen versiones a reconciliar se realiza el proceso de reconciliación de las mismas
    - Se realiza la compresión de la BBDD
    - Se realiza la reconstrucción de los índices y se actualizan las estadísticas
    
5) Tanto si se reconcilian versiones como si no se vuelve a permitir las conexiones de usuarios a la BBDD.
6) Por último se exportan las métricas del proceso al pushgateway definido.

Tendrá como datos de entrada diversos ficheros de configuración, así como los parámetros que indican la ruta de localización de estos ficheros.

Datos de entrada:

| Parámetro            | Abreviado | Explicación                                           | Default value |
|----------------------|-----------|-------------------------------------------------------|---------------|
| --db-config          | -d        | Ruta fichero db config                                |               |
| --sde-config         | -s        | Ruta fichero de salida sde config                     |               |
| --delete-config-db   | -r        | Boolean que indica si eliminar o no fichero db-config |True           |
| --logging            | -l        | Ruta del fichero logging                              |               |
| --config             | -c        | Ruta del fichero config                               |               |
| --pushgateway-config | -p        | Ruta del fichero pushgateway                          |               |


Argumentos de entrada por consola:

Fichero db_config:

| Parámetro              | Valor por defecto    | Explicación                     |
|------------------------|----------------------|---------------------------------|
| out_folder_path        | ./resources/dbconfig | Ruta de salida                  |
| out_name               | db.sde               | Nombre del fichero de salida    |
| database_platform      | POSTGRESQL           | Plataforma de la base de datos  |
| instance               | localhost            | Instancia                       |
| account_authentication | DATABASE_AUTH        | Tipo de autenticación           |
| username               | user                 | Nombre de usuario               |
| password               | changeme             | Contraseña                      |
| save_user_pass         | SAVE_USERNAME        | Política de guardado de usuario |
| database               | database             | Nombre de la base de datos      |
| version_type           |                      | Versión                         |

Fichero pushgateway:

| Parámetro | Valor por defecto   | Explicación        |
|-----------|---------------------|--------------------|
| gateway   | localhost:9000      | Puerto de salida   |
| job       | reconcile-arcgis-db | Nombre de la tarea |
| username  | username            | Nombre de usuario  |
| password  | changeme            | Contraseña         |

Fichero config (opcional):

| Parámetro           | Valor por defecto  | Explicación              |
|---------------------|--------------------|--------------------------|
| reconcile_mode      | ALL_VERSIONS       | Modo reconciliación      |
| target_version      | sde.DEFAULT        | Versión objetivo         |
| acquire_locks       | NO_LOCK_ACQUIRED   | Modo gestión cerraduras  |
| abort_if_conflicts  | NO_ABORT           | Gestión si conflictos    |
| conflict_definition | BY_OBJECT          | Definición de conflictos |
| conflict_resolution | FAVOR_EDIT_VERSION | Resolución de conflictos |
| with_post           | POST               | Acción en caso de post   |
| with_delete         | KEEP_VERSION       | Acción en caso de delete |