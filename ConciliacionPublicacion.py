import arcpy
import logging
from datetime import date
from datetime import datetime
print("modulos cargados")

# Calculo de fecha
fecha = date.today()
now = datetime.now()
hora = now.hour
minutos = now.minute
segundos = now.second

# Variables
adminConn = r"C:/Users/apais/Desktop/vente_shs@sde.sde" #conexion sde al usuario administrador de la geodatabase
out_resumen = "C:/Users/apais/Desktop/reconciliacionlog_{}_{}{}{}.txt".format(fecha, hora, minutos, segundos) #archivo de salida del proceso de conciliacion y publicacion

# Lista de usuarios conectados
userList = arcpy.ListUsers(adminConn)
userNames = [u.Name for u in userList]
print("Usuarios conectados: " + str(userNames))

# Bloquear nuevas conexiones a la BBDD
try:
    arcpy.AcceptConnections(adminConn, False)
    print("Nuevas conexiones a BBDD bloqueadas")
except TypeError as e:
    logging.error("TypeError", e)

# Tiempo de espera de cortecía para la desconexión de usuarios conectados en el momento de ejecucion
#time.sleep(900)

# Desconectando a todos los usuarios de la BBDD
try:
    arcpy.DisconnectUser(adminConn, "ALL")
    print("Usuarios de la BBDD desconectados")
    userList = arcpy.ListUsers(adminConn)
    userNames = [u.Name for u in userList]
    print("Usuarios conectados: " + str(userNames))
except TypeError as e:
    logging.error("TypeError", e)

try:
    ### Obtención de las versiones a reconciliar
    print("Compilando la lista de versiones que se reconciliaran")
    verList = arcpy.da.ListVersions(adminConn)
    versionList = [ver.name for ver in verList if ver.parentVersionName == 'sde.DEFAULT']
    print("versiones listdas: " + str(versionList))
except TypeError as e:
    logging.error("TypeError", e)

# Realización de la reconciliacion
try:
    print("Reconciliando todas las versioness obtenidas...")
    arcpy.ReconcileVersions_management(adminConn, "ALL_VERSIONS", "sde.DEFAULT", versionList, "NO_LOCK_ACQUIRED", "NO_ABORT", "BY_OBJECT", "FAVOR_EDIT_VERSION", "POST", "KEEP_VERSION", out_resumen)
    print("Proceso de reconciliado y publicado de las versiones realizado")
except TypeError as e:
    logging.error("TypeError", e)

# Restaurar los permisos de conexion a la BBDD
arcpy.AcceptConnections(adminConn, True)
print("Permiso de conexion a BBDD restablecido")

print("Proceso finalizado")